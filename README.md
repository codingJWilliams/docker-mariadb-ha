# mariadb-ha

A docker container based on library/mariadb which implements a custom leadership election protocol in order to provide high availability when deployed inside of a nomad & consul cluster.

## Algorithm

 - Each MySQL container fetches a consul kv value of which instance is currently the leader, and when that instance last checked in
 - If we're currently the leader, or if the checkin time of the current leader is more than GRACE_PERIOD seconds ago
    1. Update consul kv store to say that we are the leader
    2. Update consul kv store checkin time
    3. Double check that we're still the leader
        a. If we're not, wait a random backoff period then go back to the first step of the algorithm
    4. If MySQL is currently in slave mode, stop the slave process and disable read-only mode
 - Else
    1. If we're already replicating from a different leader than is recorded in consul kv
        a. Stop the previous slave process
        b. Start replicating from the new leader
    2. If we're not already replicating
        a. Start replicating from the new leader
 - Wait a random period of 0-2 seconds before the next loop of the algorithm