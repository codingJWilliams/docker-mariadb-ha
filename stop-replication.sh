replication_user=$1
replication_password=$2

MYSQL="mariadb -h 127.0.0.1 -u $replication_user -p$replication_password"

while ! $MYSQL -e 'select 1;'; do
    sleep 2
done;

$MYSQL -e 'STOP ALL SLAVES;'
$MYSQL -e 'RESET SLAVE ALL;'
$MYSQL -e 'SELECT @@global.gtid_binlog_pos;'
$MYSQL -e 'SET @@global.read_only=0'