leader_ip=$1
leader_port=$2
replication_user=$3
replication_password=$4


MASTER_OPTIONS="-h $leader_ip --port $leader_port -u $replication_user -p$replication_password"
MASTER="mariadb $MASTER_OPTIONS"
SLAVE="mariadb -h 127.0.0.1 -u $replication_user -p$replication_password"

echo "[election] Checking slave connectivity"
while ! $SLAVE -e 'select 1;'; do
    sleep 2
    echo "[election] slave failed..."
done;
echo "[election] Slave connected"

echo "[election] Checking master connectivity"
while ! $MASTER -e 'select 1;'; do
    sleep 2
    echo "[election] master failed..."
done;
echo "[election] Master connected"

bash ./stop-replication.sh "$replication_user" "$replication_password"

# Import replication data
log_data="$(( mariadb-dump $MASTER_OPTIONS --all-databases --master-data=2 | tee >($SLAVE) ) | grep 'CHANGE MASTER TO')"
log_file="$(echo $log_data | cut -d"'" -f2)"
log_pos="$(echo $log_data | cut -d"=" -f3 | cut -d';' -f1)"

$SLAVE -e 'SET @@global.read_only=1'
$SLAVE -e "CHANGE MASTER TO MASTER_HOST='$leader_ip', MASTER_PORT=$leader_port, MASTER_USER='$replication_user', MASTER_PASSWORD='$replication_password', MASTER_CONNECT_RETRY=10, MASTER_LOG_FILE='$log_file', MASTER_LOG_POS=$log_pos;"
$SLAVE -e "START SLAVE;"