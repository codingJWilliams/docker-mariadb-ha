from consul_kv import Connection
from urllib.error import HTTPError
import os
import time
import logging
import random

# create logger
logger = logging.getLogger('election')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

conn = Connection(endpoint=os.environ.get("CONSUL_ENDPOINT", "http://localhost:8500/v1/"))

instance = os.environ.get("INSTANCE")
namespace = os.environ.get("NAMESPACE", "mariadb")
grace_period = int(os.environ.get("GRACE_PERIOD", "5"))

my_addr = os.environ.get("MY_ADDR")
replication_user = os.environ.get("REPLICATION_USER")
replication_password = os.environ.get("REPLICATION_PASSWORD")


replicating = False
replicating_from = None

logger.info("Running with settings:")
logger.info(f"instance = {instance}")
logger.info(f"namespace = {namespace}")
logger.info(f"grace_period = {grace_period}")

def get_key(conn, key, default = None):
    global namespace
    try:
        return conn.get(f"election/{namespace}/{key}")[f"election/{namespace}/{key}"]
    except HTTPError:
        return default

def start_replicating(target, leader_address):
    global replicating
    global replicating_from
    replicating = True
    replicating_from = target
    logger.info(f"Beginning replication (source={target})")
    leader_ip = leader_address.split(":")[0]
    leader_port = leader_address.split(":")[1]
    os.system(f"/bin/bash start-replication.sh '{leader_ip}' '{leader_port}' '{replication_user}' '{replication_password}'")

def stop_replicating():
    global replicating
    replicating = False
    replicating_from = None
    logger.info("Stopping replication")
    os.system(f"/bin/bash stop-replication.sh '{replication_user}' '{replication_password}'")

# Stop replicating on startup, just in case we are
stop_replicating()

# def take_over():
#     logger.info("Taking over leadership")
  
while True:
    time.sleep(random.random() * 2)
    # Check if we need to become a leader
    current_leader = get_key(conn, f"leader")
    current_leader_address = get_key(conn, f"leader_addr")
    last_check_in = float(get_key(conn, f"leader_checkin", 0))

    if (last_check_in < ((time.time_ns()/1000000) - (grace_period*1000))) or current_leader == instance:
        # Become leader / refresh leadership bid
        conn.put(f"election/{namespace}/leader", instance)
        conn.put(f"election/{namespace}/leader_addr", my_addr)
        conn.put(f"election/{namespace}/leader_host", my_addr.split(":")[0])
        conn.put(f"election/{namespace}/leader_port", my_addr.split(":")[1])
        conn.put(f"election/{namespace}/leader_checkin", time.time_ns()/1000000)
        # Check we're still leader
        if get_key(conn, f"leader") != instance:
            logger.warning("Synchronisation error - I requested to be leader but someone else did at the same time!")
            backoff = float(round(random.random() * 3, 2))
            logger.warning(f"Backing off for {backoff} seconds")
            time.sleep(backoff)
            continue

        # Make sure we're not replicating from another host
        if replicating: 
            logger.info("Switching to leader mode")
            stop_replicating()
    else:
        # Someone else has an active leader bid
        if replicating and current_leader != replicating_from:
            logger.info(f"Leader changed, changing replication source")
            stop_replicating()
            start_replicating(current_leader, current_leader_address)
        elif not replicating: 
            logger.info("Switching to slave mode")
            start_replicating(current_leader, current_leader_address)
