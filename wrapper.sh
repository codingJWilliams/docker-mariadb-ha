#!/bin/bash
echo "$INSTANCE IS THING"
echo "CMDLINE IS " mariadbd --server-id=$(echo "$INSTANCE" | sed 's/services0//g')
sudo -u mysql MARIADB_ROOT_PASSWORD=$MARIADB_ROOT_PASSWORD /usr/local/bin/docker-entrypoint.sh --server-id=$(echo "$INSTANCE" | sed 's/services0//g') --log-bin --log-basename=master1 --binlog-format=mixed &

{
    sleep 5 # Wait for DB to boot
    while true; do
        python3 election.py
        sleep 4
    done
} &

wait -n
exit $?