FROM mariadb
COPY ./requirements.txt .
RUN apt update -y && apt install -y python3 python3-pip sudo && apt clean -y
RUN pip3 install -r requirements.txt
COPY ./wrapper.sh .
COPY ./start-replication.sh .
COPY ./stop-replication.sh .
COPY ./election.py .
# COPY ./my.cnf /etc/mysql/mariadb.conf.d/docker.conf
RUN chmod 777 wrapper.sh
CMD ["./wrapper.sh"]